FROM alpine:latest

ENV PATH=$PATH:/root/.local/bin

ARG BUILD_DATE
ARG TERRAFORM_VERSION=0.11.8
ARG VCS_REF

LABEL app="cd-builder" license="MIT" maintainer="gitlab.com/Antmounds" repo="https://gitlab.com/Antmounds/cd-builder" org.label-schema.build-date=$BUILD_DATE org.label-schema.name="cd-builder" org.label-schema.description="Continuous deployment devops toolkit" org.label-schema.url="https://hub.docker.com/r/antmounds/cd-builder" org.label-schema.vcs-ref=$VCS_REF org.label-schema.vcs-url="https://gitlab.com/Antmounds/cd-builder" org.label-schema.vendor="Antmounds" org.label-schema.version="1.0.0" org.label-schema.schema-version="1.0"

# --no-cache: download package index on-the-fly, no need to cleanup afterwards
# --virtual: bundle packages, remove whole bundle at once, when done
RUN apk -v --update --no-cache --virtual add \
	bash \
	git \
	python \
	py-pip \
	openrc \
	openssh \
	docker; \
	pip install --upgrade pip && \
	pip install pip awscli docker-compose --upgrade --user && \
	rm -rf /var/cache/apk/* && \

	# Terraform
	wget -q https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
	unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
	rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
	mv terraform /usr/local/bin && \

	apk -v info && \

	# Start Docker on boot
	rc-update add docker boot

VOLUME /root/.aws
ENTRYPOINT /bin/sh